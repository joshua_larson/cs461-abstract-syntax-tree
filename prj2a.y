%{
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>

extern char yytext[];
extern int yylineno;

int yylex(void);
void yyerror(char *s);
%}

%token CLASS PUBLIC STATIC VOID MAIN STRING EXTENDS RETURN
%token  BOOLEAN IF NEW ELSE WHILE LENGTH INT TRUE FALSE THIS
%token AND ID INTEGER PRINTLN

%right '='
%left '{'
%left AND
%left '+'
%left '-'
%left '*'
%left '.'
%left '!'
%nonassoc '<'

%start Program

%%
Program         
    :   MainClass ClassDeclPrime {puts("Ok!");}
    ;
MainClass       
    :  CLASS ID '{' PUBLIC STATIC VOID MAIN '(' STRING '[' ']' ID ')' '{' Statement '}' '}' {puts("\tMain Class Declaration");}
    ;
ClassDeclPrime  
    :   ClassDecl ClassDeclPrime
    |
    ;
ClassDecl       
    :   CLASS ID '{' VarDeclPrime MethodDeclPrime '}'
    |   CLASS ID '{' MethodDeclPrime '}'
    |   CLASS ID EXTENDS ID '{' VarDeclPrime MethodDeclPrime '}'
    |   CLASS ID EXTENDS ID '{' MethodDeclPrime '}'
    ;
VarDeclPrime     
    :   VarDecl VarDeclPrime
    |
    ;
VarDecl         
    :   Type ID ';'  
    ;
MethodDeclPrime 
    :   MethodDecl MethodDeclPrime 
    |
    ;
MethodDecl      
    :   PUBLIC Type ID '(' ParamList ')'
       	'{' VarDeclPrime StatementPrime RETURN Exp ';' '}' 
    |	PUBLIC Type ID '(' ParamList ')' '{' StatementPrime RETURN Exp ';' '}'
    ;
ParamList
	:	Type ID ParamList
	|	',' Type ID ParamList
    |
	;
Type            
    :   INT '[' ']'
    |   BOOLEAN
    |   INT
    |   ID
    ;
StatementPrime
    :   Statement StatementPrime
    |
    ;
Statement       
    :   '{' StatementPrime '}'
    |   IF '(' Exp ')' Statement ELSE Statement
    |   WHILE '(' Exp ')' Statement
    |   PRINTLN '(' Exp ')' ';'
    |   ID '=' Exp ';'
    |   ID '[' Exp ']' '=' Exp ';'
    ;
ExpPrime
    :   Exp ExpPrime
    |   Exp ',' ExpPrime
    |
    ;
Exp
    :   Exp AND Exp
    |   Exp '<' Exp 
    |   Exp '+' Exp
    |   Exp '-' Exp
    |   Exp '*' Exp
    |   Exp '[' Exp ']'
    |   Exp '.' LENGTH
    |   Exp '.' ID '(' ExpPrime ')'
    |   INTEGER 
    |   TRUE
    |   FALSE
    |   ID
    |   THIS
    |   NEW INT '[' Exp ']'
    |   NEW ID '(' ')'
    |   '!' Exp
    |   '(' Exp ')'
    ;
%%

void yyerror(char* s){	
    fprintf(stderr, "line %d messed up because of %s\n", yylineno, s);
	}
	
int main(void) {
	yyparse();
	return 0;
}
