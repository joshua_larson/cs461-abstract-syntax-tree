Abstract Syntax Tree Generator

Creates a tree like data structure that takes code (as of now, only supports [MiniJava](http://minijava.codeplex.com/) code) as input and generates a tree-like data structure iff the code follows the syntax of MiniJava.  The program then outputs the abstract syntax tree of the code in a tabbed, line by line format.