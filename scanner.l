%{
#include <stdio.h>
#include "y.tab.h"
%}

number  [0-9]+\.?|[0-9]*\.[0-9]+
identifier  [a-z][a-zA-Z0-9_]*

%%

[ ]             { /* skip blanks */ }
{number}        { return NUMBER; }
{identifier}    { return ID; }
\n|.            { return yytext[0]; }

%%

int
yywrap()
{
    return 1;
}
