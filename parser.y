%{
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
int fileno(FILE *stream);       // kludge to avoid warning when compile -std=c99


#define ID_LEN_LIMIT    64
#define LIST_LEN_LIMIT  16

typedef struct { char s[ID_LEN_LIMIT]; } Identifier_t;


typedef struct exp Exp_t;

/* Expressions */

typedef struct { Exp_t * list_reverse[LIST_LEN_LIMIT]; int length; } ExpList_t;

typedef struct { Exp_t * e1; Exp_t * e2; } And_t;
typedef struct { Exp_t * e1; Exp_t * e2; } Less_t;
typedef struct { Exp_t * e1; Exp_t * e2; } ArrLook_t;
typedef struct { Exp_t * e; } ArrLen_t;
typedef struct { Exp_t * e1; Exp_t * e2; } Plus_t;
typedef struct { Exp_t * e1; Exp_t * e2; } Minus_t;
typedef struct { Exp_t * e1; Exp_t * e2; } Times_t;
typedef int True_t;
typedef int False_t; // Type defaults to int, but value is unused
typedef int This_t;
typedef struct { int r; } IntegerLiteral_t;
typedef struct { Exp_t * e; } Not_t;
typedef struct { Identifier_t * i; } NewObj_t;
typedef struct { Exp_t * e; } NewArr_t;
typedef struct { Exp_t * e; Identifier_t * i; ExpList_t * el; } Call_t;
typedef struct { char s[ID_LEN_LIMIT]; } IdentifierExp_t;

struct exp {
    int eu_specifier;
    union {
    	And_t and;
    	Less_t less;
    	ArrLook_t look;
    	ArrLen_t len;
        Plus_t plus;
        Minus_t minus;
        Times_t times;
        Call_t call;
        True_t tru;
        False_t fals;
        This_t ths;
        NewArr_t nar;
        NewObj_t nob;
        Not_t not;
        IntegerLiteral_t intliteral;
        IdentifierExp_t identifierexp;
        } eu;
    };

/* Statements */

typedef struct statement Statement_t;

typedef struct { Statement_t * list_reverse[LIST_LEN_LIMIT]; int length; } StatementList_t;

typedef struct { Identifier_t * i; Exp_t * e; } Assign_t;
typedef struct { StatementList_t * s1; } Block_t;
typedef struct { Exp_t * e; Statement_t * s1; Statement_t * s2; } If_t;
typedef struct { Exp_t * e; Statement_t * s; } While_t;
typedef struct { Exp_t * e; } Print_t;
typedef struct { Identifier_t * i; Exp_t * e1; Exp_t * e2; } ArrayAssign_t;
struct statement {
    int su_specifier;
    union {
        Assign_t assign;
        Block_t block;
        If_t ifis;
        While_t whileis;
        Print_t print;
        ArrayAssign_t arrassign;
        } su;
    };

// Not sure if I should put stuff into these structs? (true false, etc)

/* Types */

typedef struct type Type_t;
typedef int IntArray_t;
typedef int Boolean_t; //Unused as above
typedef int Integer_t;
typedef struct { char s[ID_LEN_LIMIT]; } IdType_t;
struct type {
	int type_specifier;
	union {
		IntArray_t intArr;
		Boolean_t boolean;
		Integer_t integer;
		IdType_t idType;
		} tu;
	};
	
/* Classes */

//Formal
typedef struct { Type_t * t; Identifier_t * i; } Formal_t;

//FormalList
typedef struct { Formal_t * list_reverse[LIST_LEN_LIMIT]; int length; } FormalList_t;

//Variable Declaration
typedef struct { Type_t * t; Identifier_t * i; } VarDecl_t;

//Variable Declaration List
typedef struct { VarDecl_t * list_reverse[LIST_LEN_LIMIT]; int length; } VarDeclList_t;

//Method Declaration
typedef struct { Type_t * t; Identifier_t * i; FormalList_t * fl; VarDeclList_t * vl; StatementList_t * sl; Exp_t * e; } MethodDecl_t;

//Method Declaration List
typedef struct { MethodDecl_t * list_reverse[LIST_LEN_LIMIT]; int length; } MethodDeclList_t;


//Class Declaration Struct
typedef struct cDecl ClassDecl_t;

//Simple Class Declaration
typedef struct { Identifier_t * i; VarDeclList_t * vl; MethodDeclList_t * ml;} ClassDeclSimple_t;

//Extended Class Declaration
typedef struct { Identifier_t * i; Identifier_t * j; VarDeclList_t * vl; MethodDeclList_t * ml;} ClassDeclExtends_t;

//Main Class Declaration
typedef struct { Identifier_t * i1; Identifier_t * i2; Statement_t * s; } MainClass_t;

struct cDecl {
	int class_specifier;
	union {
		ClassDeclSimple_t cds;
		ClassDeclExtends_t cde;
        } cu;
	}; 

//Class Declaration List
typedef struct { ClassDecl_t * list_reverse[LIST_LEN_LIMIT]; int length; } ClassDeclList_t;

typedef struct { MainClass_t * m; ClassDeclList_t * cl; } Program_t;

// "ast" stands for "abstract syntax tree"

void display_ast_s(Statement_t * stmt, int indent);
void display_ast_sl(StatementList_t * sl, int indent);
void display_ast_e(Exp_t * expr, int indent);
void display_ast_p(MainClass_t * mc, ClassDeclList_t * cdl, int indent);
void display_ast_mc(MainClass_t * mc, int indent);
void display_ast_c(ClassDecl_t * cd, int indent);
void display_ast_cl(ClassDeclList_t * cdl, int indent);
void display_ast_m(MethodDecl_t * md, int indent);
void display_ast_ml(MethodDeclList_t * mdl, int indent);
void display_ast_f(Formal_t * f, int indent);
void display_ast_fl(FormalList_t * fl, int indent);
void display_ast_t(Type_t * t, int indent);
void display_ast_vl(VarDeclList_t * vl, int indent);
void display_ast_v(VarDecl_t * vd, int indent);
//void display_ast_t(); Type
//void display_ast_c(); Class      DO I NEED THESE?
//void display_ast(); Overall

#define SPACES_PER_INDENT       4


// Documentation for |yacc| indicates it chooses numbers for token symbols
// starting at 257; such comprise: NUMBER, ID, UMINUS
// Then ASCII numbers won't collide with those! (kludge)
enum {
    //Statement Specifiers
    PLUS_SPECIFIER = '+',
    MINUS_SPECIFIER = '-',
    TIMES_SPECIFIER = '*',
    NUMBER_SPECIFIER = '4',
	LESSTHAN_SPECIFIER = '<',
    CALL_SPECIFIER = 'c',
    IDENTIFIEREXP_SPECIFIER = 'H',
    LOOKUP_SPECIFIER = 'l',
    EXPLIST_SPECIFIER = ',',
    ASSIGN_SPECIFIER = '=',
    AND_SPECIFIER = '&',
    NOT_SPECIFIER = '!',
    ARRASSIGN_SPECIFIER = 'a',
    IFELSE_SPECIFIER = 'i',
    WHILE_SPECIFIER = 'w',
    PRINT_SPECIFIER = 'p',
    NEWOBJ_SPECIFIER = 'o',
    NEWARRAY_SPECIFIER = 'r',
    BLOCK_SPECIFIER = '6',
    LEN_SPECIFIER = '#',
    TRUE_SPECIFIER = 't',
    FALSE_SPECIFIER = 'f',
    THIS_SPECIFIER = '@',
    //Class Specifiers
    MAINCLASS_SPECIFIER = 'm',
    CDLIST_SPECIFIER = '^',
    CLASSDECLS_SPECIFIER = 's',
    CLASSDECLE_SPECIFIER = 'e',
    METHODDECL_SPECIFIER = 'd',
    FORMAL_SPECIFIER = '_',
    FL_SPECIFIER = '/',
    STATEMENTLIST_SPECIFIER = '>',
    METHODDECLLIST_SPECIFIER = '$',
    VARDECLLIST_SPECIFIER = '(',
    VARDECL_SPECIFIER = ')',
    //Type Specifiers
    BOOLTYPE_SPECIFIER = 'b',
    INTTYPE_SPECIFIER = '1',
    INTARRAYTYPE_SPECIFIER = '9',
    IDTYPE_SPECIFIER = 'x',
    ID_SPECIFIER = '8'
    };
    

%}


%union {
    MainClass_t* mcval;
    Formal_t* fval;
    FormalList_t* flval;
    MethodDecl_t* mval;
    MethodDeclList_t* mlval;
    VarDecl_t* vval; 
    VarDeclList_t* vlval;
	ClassDecl_t* cval;
	ClassDeclList_t * clval;
    Statement_t* sval;
    StatementList_t* slval;
    ExpList_t* elval;
    Exp_t* eval;
    Type_t* tval;
    Identifier_t* ival;
    }
%type <sval> Statement
%type <slval> StatementPrime
%type <mval> MethodDecl
%type <mlval> MethodDeclPrime
%type <ival> Iden
%type <elval> ExpList ExpPrime
%type <eval> Exp ExpRest
%type <mcval> MainClass
%type <fval> Formal FormalRest
%type <flval> FormalList FormalPrime
%type <vval> VarDecl
%type <vlval> VarDeclPrime
%type <cval> ClassDecl
%type <clval> ClassDeclPrime
%type <tval> Type

%token CLASS PUBLIC STATIC VOID MAIN STRING EXTENDS RETURN
%token  BOOLEAN IF NEW ELSE WHILE LENGTH INT TRUE FALSE THIS
%token AND ID INTEGER PRINTLN

%right '='
%left '{'
%left AND
%left '+'
%left '-'
%left '*'
%left '.'
%left '!'
%nonassoc '<'

%start Program

%%
Program         
    :   MainClass ClassDeclPrime { display_ast_p($1, $2, 0);}
    ;
MainClass       
    :  CLASS Iden '{' PUBLIC STATIC VOID MAIN '(' STRING '[' ']' Iden ')' '{' Statement '}' '}'
                {
                    $$ = malloc(sizeof *$$);
                    $$->i1 = $2;
                    $$->i2 = $12;
                    $$->s = $15;
                }
    ;
ClassDeclPrime  
    :   ClassDecl ClassDeclPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |           
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;
ClassDecl       
    :   CLASS Iden '{' VarDeclPrime MethodDeclPrime '}'
                {
                    $$ = malloc(sizeof *$$);
                    $$->class_specifier = CLASSDECLS_SPECIFIER;
                    $$->cu.cds.i = $2;
                    $$->cu.cds.vl = $4;
                    $$->cu.cds.ml = $5;
                }
    |   CLASS Iden EXTENDS Iden '{' VarDeclPrime MethodDeclPrime '}'
                {
                    $$ = malloc(sizeof *$$);
                    $$->class_specifier = CLASSDECLE_SPECIFIER;
                    $$->cu.cde.i = $2;
                    $$->cu.cde.j = $4;
                    $$->cu.cde.vl = $6;
                    $$->cu.cde.ml = $7;
                }
    ;
VarDeclPrime     
    :   VarDecl VarDeclPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;

VarDecl         
    :   Type Iden ';'
                {
                    $$ = malloc(sizeof *$$);
                    $$->t = $1;
                    $$->i = $2;
                }
    ;

MethodDeclPrime 
    :   MethodDecl MethodDeclPrime 
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;
MethodDecl      
    :   PUBLIC Type Iden '(' FormalList ')'
       	'{' VarDeclPrime StatementPrime RETURN Exp ';' '}' 
                {
                    $$ = malloc(sizeof *$$);
                    $$->t = $2;
                    $$->i = $3;
                    $$->fl = $5;
                    $$->vl = $8;
                    $$->sl = $9;
                    $$->e = $11;
                }
    ;
Formal
    :   Type Iden
                {
                    $$ = malloc(sizeof *$$);
                    $$->t = $1;
                    $$->i = $2;
                }
    ;
FormalRest
    :   ',' Formal
                {
                    $$ = $2;
                }
    ;
FormalPrime
    :   FormalRest FormalPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;
FormalList
	:	Formal FormalPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
	;
Type            
    :   INT '[' ']' 
                {
                    $$ = malloc(sizeof *$$);
                    $$->type_specifier = INTARRAYTYPE_SPECIFIER;
                }
    |   BOOLEAN
                {
                    $$ = malloc(sizeof *$$);
                    $$->type_specifier = BOOLTYPE_SPECIFIER;
                }
    |   INT
                {
                    $$ = malloc(sizeof *$$);
                    $$->type_specifier = INTTYPE_SPECIFIER;
                }
    |   ID
                {
                    $$ = malloc(sizeof *$$);
                    $$->type_specifier = IDTYPE_SPECIFIER;
                    strcpy($$->tu.idType.s, yytext);
                }
    ;
StatementPrime
    :   Statement StatementPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;
Statement       
    :   '{' StatementPrime '}'
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = BLOCK_SPECIFIER;
                    $$->su.block.s1 = $2;
                }
    |   IF '(' Exp ')' Statement ELSE Statement
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = IFELSE_SPECIFIER;
                    $$->su.ifis.e = $3;
                    $$->su.ifis.s1 = $5;
                    $$->su.ifis.s2 = $7;
                }
    |   WHILE '(' Exp ')' Statement
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = WHILE_SPECIFIER;
                    $$->su.whileis.e = $3;
                    $$->su.whileis.s = $5;
                }
    |   PRINTLN '(' Exp ')' ';'
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = PRINT_SPECIFIER;
                    $$->su.print.e = $3;
                }
    |   Iden '=' Exp ';'
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = ASSIGN_SPECIFIER;
                    $$->su.assign.i = $1;
                    $$->su.assign.e = $3;
                }
    |   Iden '[' Exp ']' '=' Exp ';'
                {
                    $$ = malloc(sizeof *$$);
                    $$->su_specifier = ARRASSIGN_SPECIFIER;
                    $$->su.arrassign.i = $1;
                    $$->su.arrassign.e1 = $3;
                    $$->su.arrassign.e2 = $6;
                }
    ;
ExpList
    :   Exp ExpPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    ;
ExpPrime
    :   ExpRest ExpPrime
                {
                    $$ = $2;
                    $$->list_reverse[$$->length++] = $1;
                }
    |
                {
                    $$ = malloc(sizeof *$$);
                    $$->length = 0;
                }
    ;
ExpRest
    :   ',' Exp
                {
                    $$ = $2;
                }
    ;
Exp
    :   Exp AND Exp
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = AND_SPECIFIER;
                    $$->eu.and.e1 = $1;
                    $$->eu.and.e2 = $3;
                }
    |   Exp '<' Exp
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = LESSTHAN_SPECIFIER;
                    $$->eu.less.e1 = $1;
                    $$->eu.less.e2 = $3;
                }
    |   Exp '+' Exp
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = PLUS_SPECIFIER;
                    $$->eu.plus.e1 = $1;
                    $$->eu.plus.e2 = $3;
                }
    |   Exp '-' Exp
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = MINUS_SPECIFIER;
                    $$->eu.minus.e1 = $1;
                    $$->eu.minus.e2 = $3;
                }
    |   Exp '*' Exp
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = TIMES_SPECIFIER;
                    $$->eu.times.e1 = $1;
                    $$->eu.times.e2 = $3;
                }
    |   Exp '[' Exp ']'
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = LOOKUP_SPECIFIER;
                    $$->eu.look.e1 = $1;
                    $$->eu.look.e2 = $3;
                }
    |   Exp '.' LENGTH
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = LEN_SPECIFIER;
                    $$->eu.len.e = $1;
                }
    |   Exp '.' Iden '(' ExpList ')'
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = CALL_SPECIFIER;
                    $$->eu.call.e = $1;
                    $$->eu.call.i = $3;
                    $$->eu.call.el = $5;
                }
    |   INTEGER 
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = NUMBER_SPECIFIER;
                    sscanf(
                        yytext,
                        "%d",
                        &$$->eu.intliteral.r
                        );
                }
    |   TRUE
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = TRUE_SPECIFIER;

                }
    |   FALSE
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = FALSE_SPECIFIER;
                }
    |   ID
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = IDENTIFIEREXP_SPECIFIER;
                    strcpy($$->eu.identifierexp.s, yytext);
                }
    |   THIS    
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = THIS_SPECIFIER;
                }
                
    |   NEW INT '[' Exp ']'
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = NEWARRAY_SPECIFIER;
                    $$->eu.nar.e = $4;
                }
    |   NEW Iden '(' ')'
                {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = NEWOBJ_SPECIFIER;
                    $$->eu.nob.i = $2;
                }
    |   '!' Exp
               {
                    $$ = malloc(sizeof *$$);
                    $$->eu_specifier = NOT_SPECIFIER;
                    $$->eu.not.e = $2;
                }
    |   '(' Exp ')'
                {
                    $$ = $2;
                }
    ;

Iden
    :   ID
                {
                    $$ = malloc(sizeof *$$);
                    strcpy($$->s, yytext);
                }
    ;
%%

#include "lex.yy.c"

int
main()
{
    yyparse();
    putchar('\n');
}

void yyerror(char *s)
{
    fprintf(stderr, "%s\n", s);
}

char *
getName(int u_specifier)
{
    switch ( u_specifier ) {

        case PLUS_SPECIFIER:      return "Plus";
        case MINUS_SPECIFIER:     return "Minus";
        case TIMES_SPECIFIER:     return "Times";
        case LESSTHAN_SPECIFIER:  return "LessThan";
        case CALL_SPECIFIER:      return "Call";
        case IDENTIFIEREXP_SPECIFIER: return "IdentifierExp";
        case LOOKUP_SPECIFIER:    return "Lookup";
        case LEN_SPECIFIER:       return "LengthOf";
        case NUMBER_SPECIFIER:    return "Integer Literal";
        case EXPLIST_SPECIFIER:   return "ExpressionList";
        case ASSIGN_SPECIFIER:    return "Assign";
        case AND_SPECIFIER:       return "And";
        case NOT_SPECIFIER:       return "Not";
        case ARRASSIGN_SPECIFIER: return "Array Assign";
        case TRUE_SPECIFIER:      return "True";
        case FALSE_SPECIFIER:     return "False";
        case THIS_SPECIFIER:      return "This";
        case IFELSE_SPECIFIER:    return "If";
        case WHILE_SPECIFIER:     return "While";
        case PRINT_SPECIFIER:     return "Print";
        case NEWOBJ_SPECIFIER:    return "New Object";
        case NEWARRAY_SPECIFIER:    return "New Array";
        //Class Specifiers
        case MAINCLASS_SPECIFIER: return "Main Class Declaration";
        case CDLIST_SPECIFIER:     return "Class Declaration List";
        case CLASSDECLS_SPECIFIER:return "Simple Class Declaration";
        case METHODDECLLIST_SPECIFIER: return "Method Declaration List";
        case CLASSDECLE_SPECIFIER:return "Extended Class Declaration";
        case METHODDECL_SPECIFIER:return "Method Declaration";
        case FORMAL_SPECIFIER:    return "Formal";
        case STATEMENTLIST_SPECIFIER: return "Statement List";
        case FL_SPECIFIER:        return "FormalList";
        case VARDECLLIST_SPECIFIER: return "Variable Declaration List";
        case VARDECL_SPECIFIER:   return "Variable Declaration";
        case INTARRAYTYPE_SPECIFIER: return "Int Array Type";
        case BOOLTYPE_SPECIFIER:  return "Boolean Type";
        case INTTYPE_SPECIFIER:   return "Integer Type";
        case IDTYPE_SPECIFIER:    return "ID Type";
        case ID_SPECIFIER:        return "ID";
        default:
            fprintf(
                stderr,
                "\ninternal error: getName(u_specifier=%i)\n",
                u_specifier
                );
            return "";
        }
}

void
output_toString0(int u_specifier, void * p)
{
    printf("%s@%p", getName(u_specifier), p);
}

#define output_toString0_i(iden)  output_toString0(ID_SPECIFIER, iden)
#define output_toString0_e(expr)  output_toString0((expr)->eu_specifier, expr)
#define output_toString0_s(stmt)  output_toString0((stmt)->su_specifier, stmt)
#define output_toString0_el(el)   output_toString0(EXPLIST_SPECIFIER, el)
#define output_toString0_mc(mc)	  output_toString0(MAINCLASS_SPECIFIER, mc)
#define output_toString0_c(c)     output_toString0((c)->class_specifier, c)
#define output_toString0_cl(cl)   output_toString0(CDLIST_SPECIFIER, cl)
#define output_toString0_f(f)     output_toString0(FORMAL_SPECIFIER, f)
#define output_toString0_fl(fl)   output_toString0(FL_SPECIFIER, fl)
#define output_toString0_ml(mdl) output_toString0(METHODDECLLIST_SPECIFIER, mdl)
#define output_toString0_m(md)    output_toString0(METHODDECL_SPECIFIER, md)
#define output_toString0_t(t)      output_toString0(t->type_specifier, t)
#define output_toString0_sl(sl)    output_toString0(STATEMENTLIST_SPECIFIER, sl)
#define output_toString0_vl(vdl)   output_toString0(VARDECLLIST_SPECIFIER, vdl)
#define output_toString0_v(v)      output_toString0(VARDECL_SPECIFIER, v)

void
display_ast_i(Identifier_t * iden, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ;  i-- > 0 ;  )
        putchar(' ');
    output_toString0_i(iden);
    printf("[s=\"%s\"]\n", iden->s);
}

void
display_ast_el(ExpList_t * expl, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ;  i-- > 0 ;  )
        putchar(' ');
    output_toString0_el(expl);
    //printf("[s=%s]\n", iden->s);
    if ( expl->length == 0 )
        putchar('[');
    for ( char i = expl->length, sep = '[' ;  i-- > 0 ;  sep = ',' ) {
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_e(expl->list_reverse[i]);
        }
    puts("]");
    for ( int i = expl->length ;  i-- > 0 ;  )
        display_ast_e(expl->list_reverse[i], indent + 1);
}

void
display_ast_e(Exp_t *expr, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ;  i-- > 0 ;  )
        putchar(' ');
    output_toString0_e(expr);
    switch ( expr->eu_specifier ) {
        case NUMBER_SPECIFIER:
            printf("[r=%g]\n", expr->eu.intliteral.r);
            return;

        case PLUS_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.plus.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.plus.e2);
            puts("]");
            display_ast_e(expr->eu.plus.e1, indent + 1);
            display_ast_e(expr->eu.plus.e2, indent + 1);
            return;

        case MINUS_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.minus.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.minus.e2);
            puts("]");
            display_ast_e(expr->eu.minus.e1, indent + 1);
            display_ast_e(expr->eu.minus.e2, indent + 1);
            return;

        case TIMES_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.times.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.times.e2);
            puts("]");
            display_ast_e(expr->eu.times.e1, indent + 1);
            display_ast_e(expr->eu.times.e2, indent + 1);
            return;

        case CALL_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(expr->eu.call.e);
            fputs(",i=", stdout);
            output_toString0_i(expr->eu.call.i);
            fputs(",el=", stdout);
            output_toString0_el(expr->eu.call.el);
            puts("]");
            display_ast_e(expr->eu.call.e, indent + 1);
            display_ast_i(expr->eu.call.i, indent + 1);
            display_ast_el(expr->eu.call.el, indent + 1);
            return;

        case IDENTIFIEREXP_SPECIFIER:
            printf("[s=\"%s\"]\n", expr->eu.identifierexp.s);
            return;

        case AND_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.and.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.and.e2);
            puts("]");
            display_ast_e(expr->eu.and.e1, indent + 1);
            display_ast_e(expr->eu.and.e2, indent + 1);
            return;

        case LESSTHAN_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.less.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.less.e2);
            puts("]");
            display_ast_e(expr->eu.less.e1, indent + 1);
            display_ast_e(expr->eu.less.e2, indent + 1);
            return;

        case LOOKUP_SPECIFIER:
            fputs("[e1=", stdout);
            output_toString0_e(expr->eu.look.e1);
            fputs(",e2=", stdout);
            output_toString0_e(expr->eu.look.e2);
            puts("]");
            display_ast_e(expr->eu.look.e1, indent + 1);
            display_ast_e(expr->eu.look.e2, indent + 1);
            return;

        case LEN_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(expr->eu.len.e);
            puts("]");
            display_ast_e(expr->eu.len.e, indent + 1);
            return;

        case NEWARRAY_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(expr->eu.nar.e);
            puts("]");
            display_ast_e(expr->eu.nar.e, indent + 1);
            return;

        case NEWOBJ_SPECIFIER:
            fputs("[i=", stdout);
            output_toString0_i(expr->eu.nob.i);
            puts("]");
            display_ast_i(expr->eu.nob.i, indent + 1);
            return;

        case TRUE_SPECIFIER:
            fputs("[TRUE]", stdout);
            return;

        case FALSE_SPECIFIER:
            fputs("[FALSE]", stdout);
            return;

        case THIS_SPECIFIER:
            fputs("[THIS]", stdout);
            return;

        default:
            fprintf(
                stderr,
                "\ninternal error: "
                    "display_ast_e(expr=%p[eu_specifier=%i])\n",
                expr,
                expr->eu_specifier
                );
            return;
        }
}
void 
display_ast_sl(StatementList_t * sl, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_sl(sl);
    if ( sl->length == 0)
        putchar(']');
    for ( char i = sl->length, sep = '[' ; i-- >0; sep = ','){
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_s(sl->list_reverse[i]);
    }
    for ( int i = sl->length ; i-- >0;)
        display_ast_s(sl->list_reverse[i], indent + 1);
}
void
display_ast_s(Statement_t * stmt, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ;  i-- > 0 ;  )
        putchar(' ');
    output_toString0_s(stmt);
    switch ( stmt->su_specifier ) {

        case ASSIGN_SPECIFIER:
            fputs("[i=", stdout);
            output_toString0_i(stmt->su.assign.i);
            fputs(",e=", stdout);
            output_toString0_e(stmt->su.assign.e);
            puts("]");
            display_ast_i(stmt->su.assign.i, indent + 1);
            display_ast_e(stmt->su.assign.e, indent + 1);
            return;

        case ARRASSIGN_SPECIFIER:
            fputs("[i=", stdout);
            output_toString0_i(stmt->su.arrassign.i);
            fputs(",e1=", stdout);
            output_toString0_e(stmt->su.arrassign.e1);
            fputs(",e2=", stdout);
            output_toString0_e(stmt->su.arrassign.e2);
            puts("]");
            display_ast_i(stmt->su.arrassign.i, indent + 1);
            display_ast_e(stmt->su.arrassign.e1, indent + 1);
            display_ast_e(stmt->su.arrassign.e2, indent +1);
            return;

        case IFELSE_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(stmt->su.ifis.e);
            fputs(", s1=", stdout);
            output_toString0_s(stmt->su.ifis.s1);
            fputs(", s2=", stdout);
            output_toString0_s(stmt->su.ifis.s2);
            puts("]");
            display_ast_e(stmt->su.ifis.e, indent + 1);
            display_ast_s(stmt->su.ifis.s1, indent + 1);
            display_ast_s(stmt->su.ifis.s2, indent + 1);
            return;

        case WHILE_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(stmt->su.whileis.e);
            fputs(", s=", stdout);
            output_toString0_s(stmt->su.whileis.s);
            puts("]");
            display_ast_e(stmt->su.whileis.e, indent + 1);
            display_ast_s(stmt->su.whileis.s, indent + 1);
            return;

        case PRINT_SPECIFIER:
            fputs("[e=", stdout);
            output_toString0_e(stmt->su.print.e);
            puts("]");
            display_ast_e(stmt->su.print.e, indent + 1);
            return;

        default:
            fprintf(
                stderr,
                "\ninternal error: "
                    "display_ast_s(stmt=%p[su_specifier=%i])\n",
                stmt,
                stmt->su_specifier
                );
            return;
        }
}

void
display_ast_fl(FormalList_t * fl, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_fl(fl);
    if ( fl->length == 0)
        putchar(']');
    for ( char i = fl->length, sep = '[' ; i-- >0; sep = ','){
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_f(fl->list_reverse[i]);
    }
    for ( int i = fl->length ; i-- >0;)
        display_ast_f(fl->list_reverse[i], indent + 1);

}

void
display_ast_f(Formal_t * f, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_f(f);
    fputs("[t=", stdout);
    output_toString0_t(f->t);
    fputs(",i=", stdout);
    output_toString0_i(f->i);
    display_ast_t(f->t, indent + 1);
    display_ast_i(f->i, indent + 1);
}

void
display_ast_t(Type_t * t, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_t(t);
    switch (t->type_specifier){
        case INTARRAYTYPE_SPECIFIER:
            fputs("Int Array", stdout);
            return;
        case BOOLTYPE_SPECIFIER:
            fputs("Boolean", stdout);
            return;
        case INTTYPE_SPECIFIER:
            fputs("Int", stdout);
            return;
        case IDTYPE_SPECIFIER:
            fputs("ID: ", stdout);
            fprintf(stdout, "TYPE: %s", t->tu.idType.s);
            return;
        default:
            fprintf(
                stderr,
                "\ninternal error: "
                    "display_ast_t(type=%p[type_specifier=%i])\n",
                t,
                t->type_specifier
                );
            return;
        }  
}
void
display_ast_ml(MethodDeclList_t * mdl, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_ml(mdl);
    if ( mdl->length == 0)
        putchar(']');
    for ( char i = mdl->length, sep = '[' ; i-- >0; sep = ','){
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_m(mdl->list_reverse[i]);
    }
    for ( int i = mdl->length ; i-- >0;)
        display_ast_m(mdl->list_reverse[i], indent + 1);
}
void
display_ast_m(MethodDecl_t * md, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_m(md);
    fputs("[t=", stdout);
    output_toString0_t(md->t);
    fputs(",i=", stdout);
    output_toString0_i(md->i);
    fputs(",fl=", stdout);
    output_toString0_fl(md->fl);
    fputs(",vl=", stdout);
    output_toString0_vl(md->vl);
    fputs(",sl=", stdout);
    output_toString0_sl(md->sl);
    fputs(",e=", stdout);
    output_toString0_e(md->e);

    display_ast_t(md->t, indent + 1);
    display_ast_i(md->i, indent + 1);
    display_ast_fl(md->fl, indent + 1);
    display_ast_vl(md->vl, indent + 1);
    display_ast_sl(md->sl, indent + 1);
    display_ast_e(md->e, indent + 1);
}

void
display_ast_vl(VarDeclList_t * vl, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_vl(vl);
    if ( vl->length == 0)
        putchar(']');
    for ( char i = vl->length, sep = '[' ; i-- >0; sep = ','){
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_v(vl->list_reverse[i]);
    }
    for ( int i = vl->length ; i-- >0;)
        display_ast_v(vl->list_reverse[i], indent + 1);
}

void
display_ast_v(VarDecl_t * v, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    fputs("[t=", stdout);
    output_toString0_t(v->t);
    fputs(",i=", stdout);
    output_toString0_i(v->i);
    puts("]");
    display_ast_t(v->t, indent + 1);
    display_ast_i(v->i, indent + 1);

}

void
display_ast_cl(ClassDeclList_t * c, int indent)
{
    for (int i = indent * SPACES_PER_INDENT ; i-- >0;)
        putchar(' ');
    output_toString0_cl(c);
    if (c->length == 0)
        putchar('[');
    for ( char i = c->length, sep = '[' ; i-- > 0; sep = ','){
        printf("%clist_reverse[%i]=", sep, i);
        output_toString0_c(c->list_reverse[i]);
        }
    puts("]");
    for ( int i = c->length ; i-- >0; )
        display_ast_c(c->list_reverse[i], indent + 1);
}
void
display_ast_c(ClassDecl_t * c, int indent)
{
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    output_toString0_c(c);
    switch ( c->class_specifier ){
        case CLASSDECLS_SPECIFIER:
            fputs("[i=", stdout);
            output_toString0_i(c->cu.cds.i);
            fputs(",vl=", stdout);
            output_toString0_vl(c->cu.cds.vl);
            fputs(",ml=", stdout);
            output_toString0_ml(c->cu.cds.ml);
            puts("]");
            display_ast_i(c->cu.cds.i, indent + 1);
            display_ast_vl(c->cu.cds.vl, indent + 1);
            display_ast_ml(c->cu.cds.ml, indent + 1);
            return;
        case CLASSDECLE_SPECIFIER:
            fputs("[i=", stdout);
            output_toString0_i(c->cu.cde.i);
            fputs(",j=", stdout);
            output_toString0_i(c->cu.cde.j);
            fputs(",vl=", stdout);
            output_toString0_vl(c->cu.cde.vl);
            fputs(",ml=", stdout);
            output_toString0_ml(c->cu.cde.ml);
            puts("]");
            display_ast_i(c->cu.cde.i, indent + 1);
            display_ast_i(c->cu.cde.j, indent + 1);
            display_ast_vl(c->cu.cde.vl, indent + 1);
            display_ast_ml(c->cu.cde.ml, indent + 1);
            return;
        default:
            fprintf(
                stderr,
                "\ninternal error: "
                    "display_ast_c(class=%p[class_specifier=%i])\n",
                c,
                c->class_specifier
                );
            return;
        }
}
void
display_ast_mc(MainClass_t * m, int indent)
{   
    for ( int i = indent * SPACES_PER_INDENT ; i-- >0; )
        putchar(' ');
    fputs("[m.i1=", stdout);
    output_toString0_i(m->i1);
    fputs(",m.i2=", stdout);
    output_toString0_i(m->i2);
    fputs(",m.s=", stdout);
    output_toString0_s(m->s);
    display_ast_i(m->i1, indent + 1);
    display_ast_i(m->i2, indent + 1);
    display_ast_s(m->s, indent + 1);
}
void
display_ast_p(MainClass_t * m, ClassDeclList_t * c ,int indent)
{
	for ( int i = indent * SPACES_PER_INDENT ; i-- > 0; )
		putchar(' ');

    fputs("[p=", stdout);
    output_toString0_mc(m);
    fputs(",c=", stdout);
    output_toString0_cl(c);
    puts("]");
    display_ast_mc(m, indent + 1);
    display_ast_cl(c, indent + 1);
    
}
