
%{
#include <stdio.h>
#include "y.tab.h"

int yylineno;
%}


delim	[ \t\n]
ws	{delim}+
digit	[0-9]
integer	{digit}+


%%
{ws}            /* Ignore Whitespace */;
"class"		    {return CLASS;}
"public"		{return PUBLIC;}
"static"		{return STATIC;}
"void"		    {return VOID;}
"main"		    {return MAIN;}
"String"		{return STRING;}
"extends"		{return EXTENDS;}
"return"		{return RETURN;}
"boolean"		{return BOOLEAN;}
"if"			{return IF;}
"new"			{return NEW;}
"else"		    {return ELSE;}
"while"		    {return WHILE;}
"length"		{return LENGTH;}
"int"			{return INT;	}
"true"		    {return TRUE;}
"false"		    {return FALSE;}
"this"		    {return THIS;}
"System.out.println"	{return PRINTLN;}
"{"			    {return '{';}
"}"			    {return '}';}
"["			    {return '[';}
"]"			    {return ']';}
";"			    {return ';';}
"("			    {return '(';}
")"			    {return ')';}
","			    {return ',';}
"+"			    {return '+';}
"="			    {return '=';}
"*"			    {return '*';}
"<"			    {return '<';}
"-"			    {return '-';}
"."			    {return '.';}
"!"			    {return '!';}
"&&"			{return AND;}
[a-zA-Z]([a-zA-Z0-9]|"_")*	{return ID;}
{integer}+		{return INTEGER;}

"/*"([^*]|\*+[^*/])*\*+"/" ;
"//".*          ;
.            {return yytext[0];}
%%

 
int
yywrap(void)
{
    return 1;
}
/*
int column = 0;

void count(){
    int i;

    for(i=0; yytext[i] != '\0'; i++)
        if(yytext[i] == 'n')
            column = 0;
        else if(yytext[i] == '\t')
            column += 8 - (column % 8);
        else
            column++;
}

int
main(void)
{
    yylex();
    return 0;
}
*/
